#include <stdio.h>
#include <stdlib.h>
#include "mpc.h"

#ifdef _WIN32
#include <string.h>

static char buffer[2048];

char* readline(char* prompt)
{
	fputs(prompt, stdout);
	fgets(buffer, 2048, stdin);
	char* cpy = malloc(strlen(buffer)+1);
	strcpy(cpy, buffer);
	cpy[strlen(cpy)-1] = '\0';
	return cpy;
}

void add_history(char* unuserd) {}

#else

#include <editline/readline.h>
#endif

typedef struct {
        int type;
        long num;
        int err;
} Nval;

enum {NVAL_NUM, NVAL_ERR};
enum {NERR_DIV_ZERO, NERR_BAD_OP, NERR_BAD_NUM};

Nval Nval_num(long x)
{
	Nval n;
	n.num = x;
	n.type = NVAL_NUM;
	return n;
}

Nval Nval_err(int x)
{
	Nval n;
	n.err = x;
	n.type = NVAL_ERR;
	return n;
}

void Nval_print(Nval n)
{
	switch(n.type)
	{
		case NVAL_NUM: printf("%li", n.num); break;
		case NVAL_ERR:
			if(n.err == NERR_DIV_ZERO)
				printf("Error: Division By Zero!");
			if(n.err == NERR_BAD_OP)
				printf("Error: Bad Operator!");
			if(n.err == NERR_BAD_NUM)
				printf("Error: Invalid Number!");
			break;
	}
}

void Nval_println(Nval n)
{
	Nval_print(n);
	putchar('\n');
}

int number_of_nodes(mpc_ast_t* t)
{
	if(t->children_num == 0) { return 1; }
	if(t->children_num >= 1)
	{
		int total = 1;
		for(int i = 0; i < t->children_num; i++)
		{
			total += number_of_nodes(t->children[i]);
		}
		return total;
	}
}

Nval eval_opr(Nval x, char* opr, Nval y)
{
	if(x.type == NVAL_ERR) { return x; }
	if(y.type == NVAL_ERR) { return y; }

	if(strcmp(opr, "+") == 0) { return Nval_num(x.num + y.num); }
	if(strcmp(opr, "-") == 0) { return Nval_num(x.num - y.num); }
	if(strcmp(opr, "*") == 0) { return Nval_num(x.num * y.num); }
	if(strcmp(opr, "/") == 0)
	{
		return y.num == 0 ? Nval_err(NERR_DIV_ZERO) : Nval_num(x.num / y.num);
	}
	return Nval_err(NERR_BAD_OP);;
}

Nval eval(mpc_ast_t* t)
{
	if(strstr(t->tag, "number")) 
	{
		errno = 0;
		long x = strtol(t->contents, NULL, 10);
		return errno != ERANGE ? Nval_num(x) : Nval_err(NERR_BAD_NUM);
	}

	char* opr = t->children[1]->contents;

	Nval x = eval(t->children[2]);

	int i = 3;

	while(strstr(t->children[i]->tag, "expr"))
	{
		x = eval_opr(x, opr, eval(t->children[i]));
		i++;
	}
	return x;
}

int main(int argc, char** argv)
{
	mpc_parser_t* Number = mpc_new("number");
	mpc_parser_t* Operator = mpc_new("operator");
	mpc_parser_t* Expr = mpc_new("expr");
	mpc_parser_t* Namadi = mpc_new("namadi");

	mpca_lang(MPCA_LANG_DEFAULT,
		"							\
			number : /-?[0-9]+/ ;				\
			operator : '-' | '+' | '/' | '*' ;		\
			expr : <number> | '(' <operator><expr>+ ')' ; 	\
			namadi : /^/ <operator> <expr>+ /$/ ;		\
		", Number, Operator, Expr, Namadi);

	puts("Namadi Language Version 0.0.1");
	puts("Press Ctrl + C to quit");

	while(1)
	{
		char* input = readline("Namadi> ");
		add_history(input);

		mpc_result_t r;
		if(mpc_parse("<stdin>", input, Namadi, &r))
		{
			//mpc_ast_print(r.output);
			Nval result = eval(r.output);
			Nval_println(result);
			mpc_ast_delete(r.output);
		}
		else
		{
			mpc_err_print(r.error);
			mpc_err_delete(r.error);
		}		

		free(input);
	}

	mpc_cleanup(4, Number, Operator, Expr, Namadi);
	return 0;
}
